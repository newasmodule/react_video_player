import React, { Component, PropTypes } from 'react';
import { Route, Link } from 'react-router-dom';
import { findDOMNode } from 'react-dom';

const linkTo = "/videoslist";
import s from './style.scss';


function trackPlayProgress(THIS) { 
  (function progressTrack() { 
    updatePlayProgress(THIS); 
    THIS.playProgressInterval = setTimeout(progressTrack, 50); 
  })(); 
}
function updatePlayProgress(THIS){ 
  THIS.playProgressBar.style.width =
    ( (THIS.videoEl.currentTime / THIS.videoEl.duration) * (THIS.progressHolder.offsetWidth) ) + "px"; 
  THIS.currenTime.innerHTML  = (THIS.videoEl.currentTime).toFixed(1);
  THIS.timeLeft.innerHTML  = (THIS.videoEl.duration - THIS.videoEl.currentTime).toFixed(1);
  THIS.duration.innerHTML  = (THIS.videoEl.duration).toFixed(1);
  console.log('updatePlayProgress ');
}
function stopTrackingPlayProgress(THIS){ 
  clearTimeout( THIS.playProgressInterval ); 
}
class PlayerContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paused: true,
    };
  }

  componentDidMount() {
    let THIS = this;
    this.videoEl = findDOMNode(this.refs.videoItem);
    this.videoControls = findDOMNode(this.refs.videoControls); 
    this.playProgressBar = findDOMNode(this.refs.playProgress);
    this.currenTime = findDOMNode(this.refs.currenTime);
    this.duration = findDOMNode(this.refs.duration);
    this.timeLeft = findDOMNode(this.refs.timeLeft);
    this.progressContainer = findDOMNode(this.refs.progress);
    this.progressHolder = findDOMNode(this.refs.progressBox);
    this.videoEl.removeAttribute('controls'); 
    this.videoEl.addEventListener('play', function() { 
      trackPlayProgress(THIS);         
    }, false);
    this.videoEl.addEventListener('pause', function() { 
      stopTrackingPlayProgress(THIS); 
    }, false);
  }
  showControls(){
    this.videoControls.style.opacity = 1; 
  }
  hideControls(){
    this.videoControls.style.opacity = 0; 
  }
  setPlayProgress(element, event){
    let THIS = this;
    let clickX = element.pageX;
    let newPercent = Math.max( 0, Math.min(1, (clickX - this.findPosX(this.progressHolder)) / this.progressHolder.offsetWidth) ); 
    THIS.currenTime.innerHTML  = (THIS.videoEl.currentTime).toFixed(1);
    THIS.timeLeft.innerHTML  = (THIS.videoEl.duration - THIS.videoEl.currentTime).toFixed(1);
    this.videoEl.currentTime = newPercent * this.videoEl.duration; 
    this.playProgressBar.style.width = newPercent * (this.progressHolder.offsetWidth) + "px"; 
  }
  setPlayProgressMouseUp(element, event){
    let THIS = this;
    this.setPlayProgress(element, event); 
  }
  findPosX(progressHolder){
    var curleft = progressHolder.offsetLeft; 
    while( progressHolder = progressHolder.offsetParent ) { 
      curleft += progressHolder.offsetLeft; 
    } 
    return curleft; 
  }
  setPaused(){
    this.setState({
      paused: true,
    });
  }
  setPlaying(){
    this.setState({
      paused: false,
    });
  }
  togglePause(e) {
    let vid = this.videoEl;
    if ( vid.paused || vid.ended ) {
      if ( vid.ended ) {
        vid.currentTime = 0;
      } 
      vid.play(); 
      this.setPlaying();
    } 
    else {
      vid.pause();
      this.setPaused();
    } 
  };
  toggleFullScreen(e) {
    let video = this.videoEl;
    if (video.requestFullscreen) {
      video.requestFullscreen();
    } else if (video.mozRequestFullScreen) {
      video.mozRequestFullScreen(); // Firefox
    } else if (video.webkitRequestFullscreen) {
      video.webkitRequestFullscreen(); // Chrome and Safari
    }
  };
  showDuration(){
    return this.videoEl && this.videoEl.duration;
  }
  showCurrentTime(){
    return this.videoEl && this.videoEl.currentTime;
  }
  showTimeLeft(){
    return this.videoEl && this.videoEl.duration - this.videoEl.currentTime;
  }

  render () {
    const { queryParams } = this.props;
    const { paused } = this.state;
    const p = queryParams && queryParams.video ||  videosList[0] ;
    const source = `/videos/${p}`
    const mute = this.togglePause.bind(this, this.state.paused);
    const fullScreen = this.toggleFullScreen.bind(this);
    const duration = this.showDuration.bind(this);
    const currenTime = this.showCurrentTime.bind(this);
    const timeLeft = this.showTimeLeft.bind(this);

    return(
      <section className={s.wrapper}>
        <div className={s.videoBox}>
          <video width="400" controls ref="videoItem" onClick={mute} onMouseOut={this.hideControls.bind(this)} onMouseOver={this.showControls.bind(this)}>
            <source src={source}/>
            <p>
              Your browser does not support HTML5 video.
            </p>
          </video>
          <div className={s.videoControls} ref="videoControls" onMouseOut={this.hideControls.bind(this)} onMouseOver={this.showControls.bind(this)}>
            <button type="button" className={s.playPause} ref="playPause" onClick={mute}>{ paused ? `Play` : 'Stop' }</button>
            <div className={s.progress} ref="progress">  
              <div className={s.progressBox} onMouseDown={this.setPlayProgress.bind(this)} onMouseUp={this.setPlayProgressMouseUp.bind(this)} ref="progressBox">  
                <div className={s.playProgress} ref="playProgress"></div>
              </div>  
                <span className={s.duration} ref="duration">
                  {duration()}
                </span>
                <span className={s.currenTime} ref="currenTime">
                  {currenTime()}
                </span>
                <span className={s.timeLeft} ref="timeLeft">
                  {timeLeft()}
                </span>
            </div> 
            <button type="button" ref="fullScreen" onClick={fullScreen}>Full-Screen</button>
            <Route
              to={linkTo}
              exact
              children={({ location: { pathname } }) => (
                <Link to={linkTo} >
                  Return to List
                </Link>
              )}
            />
          </div>
        </div>
      </section>
    );
  }
};

export default PlayerContainer;
