import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Header from 'components/Header';
import Player from 'routes/Player';
import VideosList from 'routes/VideosList';
import './style.scss';

const App = () => (
  <main>
    <Header />
    <Switch>
      <Route path="/player/:video?" component={Player} />
      <Route path="/videoslist" component={VideosList} />
      <Redirect to="/videoslist" />
    </Switch>
  </main>
);

export default App;
