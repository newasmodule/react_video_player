import React from 'react';
import s from './style.scss';
import { Route, Link } from 'react-router-dom';

// videosList it is variable, that we provided with webpack
const videoFiles = videosList.map((video, i) => {
  let route = `player/${ video }`;
  return (<div className={s.videoItem} key={video}>
            <Link to={{
              pathname: route,
              query: { video },
            }} >
                <span>{++i} video:</span> { video }
            </Link>
    </div>
  );
});

const VideosListContainer = () => (
  <section className={s.wrapper}>
    {videoFiles}
  </section>
);

export default VideosListContainer;
