import React from 'react';

import NavLink from 'components/NavLink';
import styles from './style.scss';

export default () => (
  <div className={styles.header}>
    <NavLink to="/player" label="Player" styles={styles} />
    <NavLink to="/videoslist" label="Videos List" styles={styles} />
  </div>
);
