import React, { Component } from 'react';
import Chunk from 'components/Chunk';

const loadVideosListContainer = () => import('containers/VideosList' /* webpackChunkName: "videoslist" */);

class VideosList extends Component {
  render() {
    return <Chunk load={loadVideosListContainer} />;
  }
}

export default VideosList;
