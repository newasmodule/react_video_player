import React, { Component } from 'react';
import Chunk from 'components/Chunk';

let loadPlayerContainer = () => import('containers/Player' /* webpackChunkName: "player" */);

class Player extends Component {
  render() {
    return <Chunk load={loadPlayerContainer} queryParams={this.props.location.query} />;
  }
}

export default Player;
