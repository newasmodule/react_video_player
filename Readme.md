# Player
### created and tested on node 7.9.0
so check that you are using node 7.9.0 and run `npm i`

This repo requires some videos to ensure that the task is done.
You can download the proper videos from ` http://jell.yfish.us/`
Please, plase some videos in `app/videos/`.

### To start webpack-dev-server `npm run start`
 - and navigate to `localhost:3000 in browser`

### build production version with `npm run build`
### to serve productoin result, run `npm run serve` ^^
The project uses `serve` module to serve files. It will show in console output host and port for observing result.

#### routing
This project uses a little of react-router, please don't expect this routs
will work on page refreshes ^_^. 

*** i took `https://github.com/ModusCreateOrg/budgeting-sample-app-webpack2` as starter for this app
